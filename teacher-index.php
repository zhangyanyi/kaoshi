<?php
    include dirname(__FILE__) . '/config.php';
    include dirname(__FILE__) . '/action.php';
    if(!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 2){
        header('Location:./index.php');
        exit;
    }
    ACTION::sqlInit();
    $students = ACTION::getStudentsOnlyClasses_1();
    $teacher_info = ACTION::getTeacherInfo();
    //$exams = ACTION::getExams();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/kaoshi.css">
        <script src="./js/jquery.js"></script>
        <?php
            echo '<script>console.dir('.json_encode($students).')</script>';
        ?>
    </head>
    <body class="whole">
        <div class="teacher">
            <div class="top">
                <span class="exit">退出</span>
                <span class="name"><?php echo $teacher_info['msg']['name']; ?></span>
            </div>
            <div class="left">
                <a class="button" href="./teacher-index.php">学生</a>
                <a class="button" href="./teacher-exam.php">阅卷</a>
            </div>
            <div class="right">
                <div class="caption">
                    <span class="per-3 id-card">身份证</span>
                    <span class="per-2 name">姓名</span>
                    <span class="per-1 sex">性别</span>
                    <span class="per-1 name">学号</span>
                    <span class="per-2 course">班级</span>
                    <span class="per-1 course">时间</span>
                </div>
                
                <?php
                    if($students['code']):foreach($students['msg'] as $k => $v):
                ?>
                <div class="exam-info">
                    <span class="per-3 id-card"><?php echo $v['idcard']; ?></span>
                    <span class="per-2 name"><?php echo $v['name']; ?></span>
                    <span class="per-1 sex"><?php echo sex($v['sex']); ?></span>
                    <span class="per-1 number"><?php echo $v['number']; ?></span>
                    <span class="per-2 classes"><?php echo $v['class_name']; ?></span>
                    <span class="per-1 itime"><?php echo $v['itime']; ?></span>
                </div>
                <?php
                    endforeach;endif;
                ?>
            </div>
        </div>
        <script>
            $('.exit').on('click',function(){
                location.href = './index.php';
            });
        </script>
    </body>
</html>