<?php
    include dirname(__FILE__) . '/config.php';
    include dirname(__FILE__) . '/action.php';
    ACTION::sqlInit();
    $student = ACTION::getStudentInfo();
    $course = ACTION::getCourse();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/kaoshi.css">
        <script src="./js/jquery.js"></script>
        <?php
            echo '<script>console.dir('.json_encode($student).')</script>';
        ?>
    </head>
    <body class="whole">
        <div class="student">
            <div class="student-enter table">
                <div class="td">
                    <div class="content" id="exam-begin"><img src="./img/exam-begin.png" style="width:60px;height:60px;"><span>开始考试</span></div>
                </div>
                <div class="td">
                    <a class="content" href="student-view.php"><img src="./img/exam-count.png" style="width:56px;height:56px;"><span>查看分数</span></a>
                </div>
                <!--
                <div class="td">
                    <div class="content"><img src="./img/exam-exit.png" style="width:56px;height:56px;"><span>退出考试</span></div>
                </div>
                -->
            </div>
            <div class="student-info table">
                <div class="name"><?php echo $student['msg']['name'];?></div>
                <div class="number"><?php echo $student['msg']['number'];?></div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('#exam-begin').on('click',function(){
                    var str = '<div class="mask">'
                    + '<div class="choose-course">'
                    + '<select id="select">';
                    <?php
                        if($course['code']):foreach($course['msg'] as $k => $v):
                    ?>
                    str += '<option value="<?php echo $v['id'];?>"><?php echo $v['name'];?></option>';
                    <?php
                        endforeach;endif;
                    ?>
                    str += '</select>'
                    + '<span class="submit-course">进入</span>'
                    + '</div>'
                    + '</div>';
                    $('body').append(str);
                });
                $('body').on('click','.mask',function(e){
                    console.dir(e);
                    if(e.target.className == 'mask'){
                        $(this).empty().remove();
                    }
                });
                $('body').on('click','.submit-course',function(){
                    var course_id = $('#select').val();
                    var course_name = $('#select option:selected').text();
                    location.href = './student-exam.php?course_id=' + course_id + '&course_name=' + course_name;
                });
            });
        </script>
    </body>
</html>