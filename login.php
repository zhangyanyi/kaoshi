<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="./css/kaoshi.css">
		<script src="./js/jquery.js"></script>
	</head>
	<body class="whole">
		<div class="login-interface">
			<img src="./img/kaoshi_back.png">
			<div class="opration">
				<select id="select">
					<option value="0">学生</option>
					<option value="1">教师</option>
					<option value="2">管理员</option>
				</select>
				<input type="text" placeholder="输入学号或身份证或姓名..." id="username">
				<input type="password" placeholder="输入密码...">
				<span id="submit">登录</span>
			</div>
		</div>
		<script>
			$(document).ready(function(){
				$('#select').on('change',function(){
					var placeholder = ['输入学号或身份证或姓名...','输入教师编号或姓名...','输入管理员账号或密码...'][$(this)[0].value];
					$('#username').attr('placeholder',placeholder);
				});
			});
		</script>
	</body>
</html>