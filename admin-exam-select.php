<?php
    include dirname(__FILE__) . '/config.php';
    include dirname(__FILE__) . '/action.php';
    if(!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 1){
        header('Location:./index.php');
        exit;
    }
    ACTION::sqlInit();
    $course = ACTION::getCourse();
    $getExamOfselect = ACTION::getExamOfselect();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/kaoshi.css">
        <script src="./js/jquery.js"></script>
        <script src="./js/layer/layer.js"></script>
        <?php
            echo '<script>console.dir('.json_encode($getExamOfselect).')</script>';
        ?>
    </head>
    <body class="whole">
        <div class="admin">
            <div class="top">考试管理系统</div>
            <div class="left">
                <a class="li" href="./admin-course.php">课程管理</a>
                <a class="li" href="./admin-class.php">班级管理</a>
                <a class="li" href="./admin-teacher.php">教师管理</a>
                <a class="li" href="./admin-student.php">学生管理</a>
                <a class="li on" href="./admin-exam-select.php">试题管理</a>
                <a class="li-children on" href="./admin-exam-select.php">选择题</a>
                <a class="li-children" href="./admin-exam-yesorno.php">对错题</a>
                <a class="li-children" href="./admin-exam-read.php">阅读题</a>
            </div>
            <div class="right">
                <div class="opration">
                    <div class="action">
                        <span>
                            <button id="insertExam">增加选择题</button>
                        </span>
                    </div>
                    <div class="title">
                        <span class="x1">序号</span>
                        <span class="x1">科目</span>
                        <span class="x6">问题</span>
                        <span class="x1">时间</span>
                        <span class="x1">操作</span>
                    </div>
                </div>
                <div class="content">
                    <?php if($getExamOfselect['code']):foreach($getExamOfselect['msg'] as $k => $v):?>
                    <div class="li">
                        <span class="x1"><?php echo $k + 1;?></span>
                        <span class="x1"><?php echo $v['course_name'];?></span>
                        <span class="x6" style="text-align:left;"><?php echo $v['T'];?></span>
                        <span class="x1"><?php echo $v['itime'];?></span>
                        <span class="x1 delete" data-id="<?php echo $v['id'];?>">查看</span>
                    </div>
                    <?php endforeach;endif;?>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('#insertExam').on('click',function(){
                    var str = '<div class="admin-insert">'
                    + '<div class="exam-li"><span class="exam-course">选择课程</span><select class="select-exam-type" id="select-exam-type">';
                    <?php
                        if($course['code']):foreach($course['msg'] as $k => $v):
                    ?>
                    str += '<option value="<?php echo $v['id'];?>"><?php echo $v['name'];?></option>';
                    <?php
                        endforeach;endif;
                    ?>
                    str += '</select></div>'
                    + '<div class="exam-li"><textarea class="textarea" placeholder="在此输入问题..." id="T"></textarea></div>'
                    + '<div class="exam-li"><input type="text" id="A" placeholder="在此输入正确答案..."></div>'
                    + '<div class="exam-li"><input type="text" id="B" placeholder="在此输入第二个答案..."></div>'
                    + '<div class="exam-li"><input type="text" id="C" placeholder="在此输入第三个答案..."></div>'
                    + '<div class="exam-li"><input type="text" id="D" placeholder="在此输入第四个答案..."></div>'
                    + '</div>';
                    layer.open({
                        title:'添加[选择题]',
                        type:0,
                        btn:['确定','取消'],
                        area:['240px','440px'],
                        content:str,
                        yes:function(index){
                            var course_id = $('#select-exam-type').val();
                            var course_name = $('#select-exam-type option:selected').text();
                            var R = ~~(Math.random() * 4);
                            var Y = ['A','B','C','D'][R];
                            console.info(Y);
                            var T = $('#T').val();
                            var A = $('#A').val();
                            var B = $('#B').val();
                            var C = $('#C').val();
                            var D = $('#D').val();
                            var M = [A,B,C,D];
                            var F = M[R];
                            M[R] = A;
                            M[0] = F;
                            $.post('./enter.php',{url_action:'insertExam_select',course_id:course_id,course_name:course_name,T:T,Y:Y,M:M},function(data){
                                console.dir(data);
                                if(data.code){
                                    location.reload();
                                }else{
                                    layer.msg(data.msg);
                                }
                            },'JSON');
                        }
                    });
                });
                $('.delete').on('click',function(){
                    var id = $(this)[0].dataset.id;
                    $.post('./enter.php',{url_action:'deleteExam',id:id},function(data){
                        if(data.code){
                            location.reload();
                        }else{
                            layer.msg(data.msg);
                        }
                    },'JSON');
                });
            });
        </script>
    </body>
</html>