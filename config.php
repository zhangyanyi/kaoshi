<?php
//基础配置
set_time_limit(0);
error_reporting(E_ALL);
ignore_user_abort(true);
date_default_timezone_set('PRC');
header('Content-Type:text/html; charset=utf-8');
session_start();

//本地环境

define('SQL_HOST', 'localhost');
define('SQL_PORT', 3306);
define('SQL_DB', 'kaoshi');
define('SQL_USER', 'root');
define('SQL_PWD', 'root');

//公共配置
define('SQL_CHARSET', 'utf8');
define('SQL_PERSISTENT', false);

function sex($v){
    $a = array('未知','男','女');
    return $a[$v];
}