<?php
    include dirname(__FILE__) . '/config.php';
    include dirname(__FILE__) . '/action.php';
    if(!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 2){
        header('Location:./index.php');
        exit;
    }
    ACTION::sqlInit();
    $res = ACTION::get_teacher_exams_done();
    $teacher_info = ACTION::getTeacherInfo();
    //$exams = ACTION::getExams();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/kaoshi.css">
        <script src="./js/jquery.js"></script>
        <?php
            echo '<script>console.dir('.json_encode($res).')</script>';
        ?>
    </head>
    <body class="whole">
        <div class="teacher">
            <div class="top">
                <span class="exit">退出</span>
                <span class="pass">修改密码</span>
                <span class="name"><?php echo $teacher_info['msg']['name']; ?></span>
            </div>
            <div class="left">
                <a class="button" href="./teacher-index.php">学生</a>
                <a class="button" href="./teacher-exam.php">阅卷</a>
                <a class="button" href="./teacher-done.php">已阅</a>
            </div>
            <div class="right">
                <div class="caption">
                    <span class="per-2 id-card">考生</span>
                    <span class="per-2 name">班级</span>
                    <span class="per-2 sex">课程</span>
                    <span class="per-1 name">结束时间</span>
                    <span class="per-2 course">用时</span>
                    <span class="per-1 view">状态</span>
                </div>
                
                <?php
                    if($res['code']):foreach($res['msg'] as $k => $v):
                ?>
                <div class="exam-info">
                    <span class="per-2 id-card"><?php echo $v['student_name']; ?></span>
                    <span class="per-2 name"><?php echo $v['class_name']; ?></span>
                    <span class="per-2 sex"><?php echo $v['course_name']; ?></span>
                    <span class="per-1 name"><?php echo $v['itime']; ?></span>
                    <span class="per-2 course"><?php echo floor($v['used_time'] / 60); ?>分钟</span>
                    <span class="per-1 view"><a href="./teacher-view.php?exam_id=<?php echo $v['id']; ?>">已阅</a></span>
                </div>
                <?php
                    endforeach;endif;
                ?>
            </div>
        </div>
        <script>
            $(document).on('ready',function(){
                $('.view').on('click',function(){
                    location.href = './teacher-view.php';
                });
            });
        </script>
    </body>
</html>