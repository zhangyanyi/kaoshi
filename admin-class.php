<?php
    include dirname(__FILE__) . '/config.php';
    include dirname(__FILE__) . '/action.php';
    if(!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 1){
        header('Location:./index.php');
        exit;
    }
    ACTION::sqlInit();
    $R = ACTION::getClass();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/kaoshi.css">
        <script src="./js/jquery.js"></script>
        <script src="./js/layer/layer.js"></script>
    </head>
    <body class="whole">
        <div class="admin">
            <div class="top">考试管理系统</div>
            <div class="left">
                <a class="li" href="./admin-course.php">课程管理</a>
                <a class="li on" href="./admin-class.php">班级管理</a>
                <a class="li" href="./admin-teacher.php">教师管理</a>
                <a class="li" href="./admin-student.php">学生管理</a>
                <a class="li" href="./admin-exam-select.php">试题管理</a>
                <a class="li-children" href="./admin-exam-select.php">选择题</a>
                <a class="li-children" href="./admin-exam-yesorno.php">对错题</a>
                <a class="li-children" href="./admin-exam-read.php">阅读题</a>
            </div>
            <div class="right">
                <div class="opration">
                    <div class="action">
                        <span id="insert-class">增加班级</span>
                    </div>
                    <div class="title">
                        
                    </div>
                </div>
                <div class="content">
                    <?php if($R['code']):foreach($R['msg'] as $k => $v):?>
                    <div class="panel">
                        <div class="name"><?php echo $v['name'];?></div>
                        <div class="delete" data-id="<?php echo $v['id'];?>"><span>删除</span></div>
                        <div class="time"><?php echo $v['add_time'];?></div>
                    </div>
                    <?php endforeach;endif;?>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('#insert-class').on('click',function(){
                    var str = '<div class="admin-insert">'
                    + '<div class="li"><input type="text" placeholder="班级名称..." id="class-name"></div>'
                    + '</div>';
                    layer.open({
                        title:'添加课程',
                        type:0,
                        btn:['确定','取消'],
                        area:['240px','180px'],
                        content:str,
                        yes:function(index){
                            var name = $('#class-name').val();
                            $.post('./enter.php',{url_action:'insertClass',name:name},function(data){
                                if(data.code){
                                    location.reload();
                                }else{
                                    layer.msg(data.msg);
                                }
                            },'JSON');
                        }
                    });
                    $(document).keydown('keydown',function(e){
                        if(e.keyCode == 13){
                            $('.layui-layer-btn0').click();
                        }
                    });
                });
                $('.delete').on('click',function(){
                    var id = $(this)[0].dataset.id;
                    $.post('./enter.php',{url_action:'deleteClass',id:id},function(data){
                        if(data.code){
                            location.reload();
                        }else{
                            layer.msg(data.msg);
                        }
                    },'JSON');
                });
            });
        </script>
    </body>
</html>