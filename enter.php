<?php
header('Content-type:text/html;charset=utf-8');
require(dirname(__FILE__) . '/config.php');
require(dirname(__FILE__) . '/action.php');
$url_action = trim(empty($_GET['url_action']) ?  $_POST['url_action'] : $_GET['url_action']);
$acc = $pwd = $name = $idcard = '';
$type = $sex = $course = $classes = 0;
extract($_POST);
ACTION::sqlInit();
switch($url_action){
    case 'login'://登录
        echo json_encode(ACTION::login($type,$acc,$pwd));
        exit();
        break;
    case 'sign_out'://退出
        echo json_encode(ACTION::sign_out());
        exit();
        break;
    case 'insertCourse'://添加课程
        echo json_encode(ACTION::insertCourse($name));
        exit();
        break;
    case 'insertClass'://添加班级
        echo json_encode(ACTION::insertClass($name));
        exit();
        break;
    case 'insertTeacher'://添加教师
        echo json_encode(ACTION::insertTeacher($name,$sex,$classes));
        exit();
        break;
    case 'insertStudent'://添加学生
        echo json_encode(ACTION::insertStudent($idcard,$name,$sex,$course));
        exit();
        break;
    case 'insertExam_select'://添加选择题
        echo json_encode(ACTION::insertExam_select($course_id,$course_name,$T,$Y,$M));
        exit();
        break;
    case 'insertExam_yesorno'://添加对错题
        echo json_encode(ACTION::insertExam_yesorno($course_id,$course_name,$question,$answer));
        exit();
        break;
    case 'insertExam_read'://添加阅读题
        echo json_encode(ACTION::insertExam_read($course_id,$course_name,$question));
        exit();
        break;
    case 'submitExam'://提交试卷
        echo json_encode(ACTION::submitExam($req_data,$used_time,$course_id));
        exit();
        break;
    case 'getStudentsOnlyCourse'://获取特定课程的学生
        echo json_encode(ACTION::getStudentsOnlyCourse());
        exit();
        break;
    case 'deleteCourse'://删除课程
        echo json_encode(ACTION::deleteCourse($id));
        exit();
        break;
    case 'deleteClass'://删除班级
        echo json_encode(ACTION::deleteClass($id));
        exit();
        break;
    case 'deleteTeacher'://删除教师
        echo json_encode(ACTION::deleteTeacher($id));
        exit();
        break;
    case 'deleteStudent'://删除学生
        echo json_encode(ACTION::deleteStudent($id));
        exit();
        break;
    case 'deleteExam'://删除试题
        echo json_encode(ACTION::deleteExam($id));
        exit();
        break;
    case 'getExamTest'://获取试题
        echo json_encode(ACTION::getExamTest());
        exit();
        break;
    case 'teacher_view_submit'://教师打分
        echo json_encode(ACTION::teacher_view_submit($id,$score));
        exit();
        break;
    default:
        echo json_encode(array('code' => 0,'msg' => '操作错误...'));
        exit();
        break;
}