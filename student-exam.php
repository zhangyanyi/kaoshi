<?php
    include dirname(__FILE__) . '/config.php';
    include dirname(__FILE__) . '/action.php';
    ACTION::sqlInit();
    $student = ACTION::getStudentInfo();
    $selectSubject = ACTION::getSelectSubject();
    $yesornoSubject = ACTION::getYesornoSubject();
    $readSubject = ACTION::getReadSubject();
    $sex = array('未知','男','女');
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/kaoshi.css">
        <script src="./js/jquery.js"></script>
        <script src="./js/layer/layer.js"></script>
        <?php
            echo '<script>console.dir('.json_encode($student).')</script>';
        ?>
    </head>
    <body class="whole">
        <div class="exam">
            <div class="lmenu">
                <div class="course-info">
                    <div class="who-course"><?php echo $_GET['course_name'];?>[考试]</div>
                    <div class="tips">倒计时(分钟)</div>
                    <div class="count-down" id="count-down">00:00</div>
                    <div class="begin-exam" id="submit-exam">提交试卷</div>
                </div>
                <div class="question-list" id="question-list">
                    <div class="question-box select">
                        <h3>选择题</h3>
                        <div class="question-item">
                            <span class="number">第1题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第2题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第2题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第4题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第5题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第6题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第7题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第8题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第9题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第10题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第11题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第12题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第13题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第14题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第15题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第16题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第17题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第18题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第19题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第20题</span>
                            <span class="status no">未答</span>
                        </div>
                    </div>
                    <div class="question-box yesorno">
                        <h3>对错题</h3>
                        <div class="question-item">
                            <span class="number">第1题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第2题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第3题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第4题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第5题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第6题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第7题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第8题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第9题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第10题</span>
                            <span class="status no">未答</span>
                        </div>
                    </div>
                    <div class="question-box read">
                        <h3>解答题</h3>
                        <div class="question-item">
                            <span class="number">第1题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第2题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第3题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第4题</span>
                            <span class="status no">未答</span>
                        </div>
                        <div class="question-item">
                            <span class="number">第5题</span>
                            <span class="status no">未答</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="myinfo">
                <div class="group">
                    <span class="label">身份证</span>
                    <span class="value"><?php echo $student['msg']['idcard'];?></span>
                </div>
                <div class="group">
                    <span class="label">性别</span>
                    <span class="value"><?php echo $sex[$student['msg']['sex']];?></span>
                </div>
                <div class="group">
                    <span class="label">姓名</span>
                    <span class="value"><?php echo $student['msg']['name'];?></span>
                </div>
            </div>
            <div class="rmain" id="rmain">
                <div class="big-title">选择题</div>
                <?php foreach($selectSubject['msg'] as $k => $v):?>
                <div class="subject" data-type="select" data-id="<?php echo $v['id']; ?>">
                    <div class="title"><span class="number">第<?php echo $k + 1; ?>题</span><?php echo $v['T']; ?></div>
                    <div class="result" data-answer="A"><span class="number">A:</span><span class="content"><?php echo $v['A']; ?></span></div>
                    <div class="result" data-answer="B"><span class="number">B:</span><span class="content"><?php echo $v['B']; ?></span></div>
                    <div class="result" data-answer="C"><span class="number">C:</span><span class="content"><?php echo $v['C']; ?></span></div>
                    <div class="result" data-answer="D"><span class="number">D:</span><span class="content"><?php echo $v['D']; ?></span></div>
                </div>
                <?php endforeach;?>
                <div class="big-title">对错题</div>
                <?php foreach($yesornoSubject['msg'] as $k => $v):?>
                <div class="subject" data-type="yesorno" data-id="<?php echo $v['id']; ?>">
                    <div class="title"><span class="number">第<?php echo $k + 1; ?>题</span><?php echo $v['question']; ?></div>
                    <div class="result" data-answer="yes"><span class="number">对:</span><span class="content"></span></div>
                    <div class="result" data-answer="no"><span class="number">错:</span><span class="content"></span></div>
                </div>
                <?php endforeach;?>
                <div class="big-title">解答题</div>
                <?php foreach($readSubject['msg'] as $k => $v):?>
                <div class="subject" data-type="read" data-id="<?php echo $v['id']; ?>">
                    <div class="title"><span class="number">第<?php echo $k + 1; ?>题</span><pre><?php echo $v['question']; ?></pre></div>
                    <div class="result" data-answer=""><textarea id="read_answer"></textarea></div>
                </div>
                <?php endforeach;?>
            </div>
        <script>
            $(document).ready(function(){
                var time = 1800,timer;;
                var timer2 = setInterval(function(){
                    var minute = ('00' + ~~( time / 60 )).substr(-2);
                    var secound = ('00' + ( time % 60 )).substr(-2);
                    var now_time = minute + ':' + secound;
                    if(!time){
                        clearInterval(timer2);
                    }
                    time--;
                    $('#count-down').html(now_time);
                },1000);
                var reqData = [
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0},
                    {id:0,answer:'',result:0,score:0}
                ];
                $.each($('.subject'),function(i,v){
                    reqData[i]['id'] = v.dataset.id;
                });
                console.dir(reqData);
                $('.rmain').on('click','.result',function(){
                    var index = $(this).closest('.subject').index('.subject');
                    $(this).addClass('yes').siblings('.result').removeClass('yes');
                    $('#question-list .question-item').eq(index).children('.status').removeClass('no').addClass('yes').text('已答');
                    reqData[index].id = $(this).parent('.subject')[0].dataset.id;
                    reqData[index].answer = $(this)[0].dataset.answer;
                });
                $('.rmain').on('keyup','textarea',function(){
                    var self = $(this);
                    reqData[self.closest('.subject').index('.subject')].id = self.closest('.subject')[0].dataset.id;
                    clearTimeout(timer);
                    timer = setTimeout(function(){
                        reqData[self.closest('.subject').index('.subject')].answer = self.val();
                        console.dir(reqData);
                    },1000);
                });
                $('#submit-exam').on('click',function(){
                    clearInterval(timer2);
                    console.dir(reqData);
                    $.post('./enter.php',{url_action:'submitExam',req_data:reqData,used_time:1800 - time,course_id:<?php echo $_GET['course_id'];?>},function(data){
                        console.dir(data);
                        if(data.code){
                            location.href = './student-index.php';
                        }else{
                            layer.msg(data.msg);
                        }
                    },'JSON');
                });
            });
        </script>
    </body>
</html>