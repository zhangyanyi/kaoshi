<?php
    include dirname(__FILE__) . '/config.php';
    include dirname(__FILE__) . '/action.php';
    if(!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 3){
        header('Location:./index.php');
        exit;
    }
    ACTION::sqlInit();
    $res = ACTION::getExamsOfStudent();
    $info = ACTION::getStudentInfo();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/kaoshi.css">
        <script src="./js/jquery.js"></script>
        <link href='//cdn.webfont.youziku.com/webfonts/nomal/21063/37057/58bd5bb6f629da187c87f3cf.css' rel='stylesheet' type='text/css' />
        <?php
            echo '<script>console.dir('.json_encode($info).')</script>';
        ?>
    </head>
    <body class="whole">
        <div class="student-view">
            <div class="top">
                <span class="exit">退出</span>
                <span class="name"><?php echo $info['msg']['name'];?></span>
            </div>
            <div class="title">
                <span>序号</span><span>课程</span><span>分数</span><span>用时</span><span>结束时间</span>
            </div>
            <div class="main">
                <?php if($res['code']):foreach($res['msg'] as $k => $v):?>
                <div>
                    <span><?php echo $k + 1; ?></span>
                    <span><?php echo $v['course_name']; ?></span>
                    <span><?php echo $v['score']; ?></span>
                    <span><?php echo $v['used_time']; ?>分钟</span>
                    <span><?php echo $v['itime']; ?></span>
                </div>
                <?php endforeach;endif;?>
            </div>
        </div>
        <script>
            $('.exit').on('click',function(){
                location.href = './index.php';
            });
        </script>
    </body>
</html>