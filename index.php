<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/kaoshi.css">
        <script src="./js/jquery.js"></script>
        <script src="./js/layer/layer.js"></script>
    </head>
    <body class="whole">
        <div class="login-interface">
            <h3>衡阳师范学院考试系统</h3>
            <!--<img src="./img/bg.png">-->
            <div class="opration">
                <select id="select"> 
                    <option value="3">学生</option>
                    <option value="2">教师</option>
                    <option value="1">管理员</option>
                </select>
                <!--修改 start-->
                <p>账&nbsp;&nbsp;&nbsp;号：<input type="text" placeholder="输入学号或身份证或姓名..." id="username"></p>
                <p>密&nbsp;&nbsp;&nbsp;码：<input type="password" placeholder="输入密码..." id="password"></p>
                <p>验证码 : <input name="captcha" type="text" placeholder="验证码">
                    <span class="captchaOperation" disabled type="text"></span>
                </p>
                <p><b class="captchaMsg"></b></p>
                <!--修改 end-->
                <!--<input type="text" placeholder="输入学号或身份证或姓名..." id="username">
                <input type="password" placeholder="输入密码..." id="password">-->
                <span id="submit">登&nbsp;&nbsp;&nbsp;录</span>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                var type = 3;
                $('#select').on('change',function(){
                    type = $(this).val();
                    var placeholder = ['','输入管理员账号或密码...','输入教师编号或姓名...','输入学号或身份证或姓名...'][$(this)[0].value];
                    $('#username').attr('placeholder',placeholder);
                });
                /*
                 *  验证码 start
                 */
                function randomNumber(min, max) {
                    return Math.floor(Math.random() * (max - min + 1) + min);
                };
                $(".captchaOperation").html([randomNumber(1, 100), "+", randomNumber(1, 100)].join(" "));
                $(".captchaOperation").on("click",function(){
                    $('.captchaOperation').html([randomNumber(1, 100), "+", randomNumber(1, 100)].join(" "));
                });
                var items = $(".captchaOperation").html().match(/\d+/g), sum = parseInt(items[0]) + parseInt(items[1]);
                $("input[name='captcha']").on("change focus",function(){
                    if($(this).val()==""){
                        $("b.captchaMsg").text("请输入验证码");
                    }else if($(this).val()!=sum){
                        $("b.captchaMsg").text("答案错误");
                    }else{
                        $("b.captchaMsg").empty();
                    }
                });
                /*
                 *  验证码 end
                 */
                $('#submit').on('click',function(){
                    var acc = $('#username').val();
                    var pwd = $('#password').val();
                    var captcha = $(".captchaMsg").text(); //验证码错误提示
                    if(captcha == "")//如果没有验证码错误提示
                    $.post('./enter.php',{url_action:'login',type:type,acc:acc,pwd:pwd},function(data){
                        if(data.code){
                            layer.msg(data.msg);
                            var href = ['','./admin-course.php','./teacher-index.php','./student-index.php'];
                            setTimeout(function(){location.href = href[type];},1000);
                        }else{
                            layer.msg(data.msg);
                        }
                    },'JSON')
                });
            });
        </script>
    </body>
</html>