<?php
    include dirname(__FILE__) . '/config.php';
    include dirname(__FILE__) . '/action.php';
    if(!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 1){
        header('Location:./index.php');
        exit;
    }
    ACTION::sqlInit();
    $class = ACTION::getClass();
    $all_student = ACTION::getStudent();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="./css/kaoshi.css">
		<script src="./js/jquery.js"></script>
        <script src="./js/layer/layer.js"></script>
	</head>
	<body class="whole">
		<div class="admin">
			<div class="top">考试管理系统</div>
            <div class="left">
                <a class="li" href="./admin-course.php">课程管理</a>
                <a class="li" href="./admin-class.php">班级管理</a>
                <a class="li" href="./admin-teacher.php">教师管理</a>
                <a class="li on" href="./admin-student.php">学生管理</a>
                <a class="li" href="./admin-exam-select.php">试题管理</a>
                <a class="li-children" href="./admin-exam-select.php">选择题</a>
                <a class="li-children" href="./admin-exam-yesorno.php">对错题</a>
                <a class="li-children" href="./admin-exam-read.php">阅读题</a>
            </div>
            <div class="right">
                <div class="opration">
                    <div class="action">
                        <span id="insert-student">增加学生</span>
                    </div>
                    <div class="title">
                        <span class="x1">学生学号</span>
                        <span class="x2">学生姓名</span>
                        <span class="x1">学生性别</span>
                        <span class="x3">身份证号</span>
                        <span class="x2">注册时间</span>
                        <span class="x1">学生操作</span>
                    </div>
                </div>
                <div class="content">
                    <?php if($all_student['code']):foreach($all_student['msg'] as $k => $v):?>
                    <div class="li">
                        <span class="x1"><?php echo $v['number'];?></span>
                        <span class="x2"><?php echo $v['name'];?></span>
                        <span class="x1"><?php echo sex($v['sex']);?></span>
                        <span class="x3"><?php echo $v['idcard'];?></span>
                        <span class="x2"><?php echo $v['itime'];?></span>
                        <span class="x1 delete" data-id="<?php echo $v['id'];?>">删除</span>
                    </div>
                    <?php endforeach;endif;?>
                </div>
            </div>
		</div>
        <script>
            $(document).ready(function(){
                var sex = 1;
                $('#insert-student').on('click',function(){
                    var str = '<div class="admin-insert">'
                    + '<div class="li"><input type="text" placeholder="身份证号..." id="idcard"></div>'
                    + '<div class="li"><input type="text" placeholder="学生姓名..." id="student-name"></div>'
                    + '<div class="li"><span class="sex on" data-value="1">男</span><span class="sex" data-value="2">女</span></div>'
                    + '<div class="li"><select id="select-class">'
                    <?php if($class['code']):foreach($class['msg'] as $k => $v):?>
                    + '<option value="<?php echo $v['id'];?>"><?php echo $v['name'];?></option>'
                    <?php endforeach;endif;?>
                    + '</select></div>'
                    + '</div>';
                    layer.open({
                        title:'添加学生',
                        type:0,
                        btn:['确定','取消'],
                        area:['240px','300px'],
                        content:str,
                        yes:function(index){
                            var idcard = $('#idcard').val();
                            var name = $('#student-name').val();
                            var classes = $('#select-class').val();
                            $.post('./enter.php',{url_action:'insertStudent',idcard:idcard,name:name,sex:sex,classes:classes},function(data){
                                console.dir(data);
                                if(data.code){
                                    layer.msg(data.msg);
                                    setTimeout(function(){location.reload();},1000);
                                }else{
                                    layer.msg(data.msg);
                                }
                            },'JSON');
                        }
                    });
                    $('.sex').on('click',function(){
                        $(this).addClass('on').siblings('span').removeClass('on');
                        sex = $(this)[0].dataset.value;
                    }); 
                });
                $('.delete').on('click',function(){
                    var id = $(this)[0].dataset.id;
                    $.post('./enter.php',{url_action:'deleteStudent',id:id},function(data){
                        if(data.code){
                            location.reload();
                        }else{
                            layer.msg(data.msg);
                        }
                    },'JSON');
                });
            });
        </script>
	</body>
</html>