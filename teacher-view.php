<?php
    include dirname(__FILE__) . '/config.php';
    include dirname(__FILE__) . '/action.php';
    if(!isset($_SESSION['user_type']) || $_SESSION['user_type'] != 2){
        header('Location:./index.php');
        exit;
    }
    ACTION::sqlInit();
    $res = ACTION::getExamOne($_GET['exam_id']);
    $exam = json_encode($res['msg']['req_data']);
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./css/kaoshi.css">
        <script src="./js/jquery.js"></script>
        <link href='//cdn.webfont.youziku.com/webfonts/nomal/21063/37057/58bd5bb6f629da187c87f3cf.css' rel='stylesheet' type='text/css' />
        <?php
            echo '<script>console.dir('.json_encode($res).')</script>';
        ?>
    </head>
    <body class="whole">
        <div class="teacher">
            <div class="top">
                <span class="exit">退出</span>
                <span class="name">王春蓝</span>
            </div>
            <div class="left">
                <a class="button" href="./teacher-index.php">学生</a>
                <a class="button" href="./teacher-exam.php">阅卷</a>
            </div>
            <div class="right">
                <div class="rmain" id="rmain">
                    <div class="big-title">解答题<div class="teacher-view-submit" id="teacher-view-submit">提交</div></div>
                    <?php foreach($res['msg']['req_data'] as $k => $v):?>
                    <div class="subject" data-type="read" data-id="<?php echo $v['id']; ?>">
                        <div class="title">
                            <div class="score">
                                <span class="minus"><img src="./img/minus.png"></span>
                                <span class="css948d789e55247 num">0</span>
                                <span class="plus"><img src="./img/plus.png"></span>
                            </div>
                            <?php echo $v['question']; ?>
                        </div>
                        <div class="result" data-answer="">
                            <div id="read_answer" class="teacher-view-answer">
                                <?php echo $v['answer']; ?>学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生学生
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        <script src="./js/layer/layer.js"></script>
        <script>
            $('.plus').on('click',function(){
                var num = $(this).parent().children('.num');
                var num_now = parseInt(num.text());
                if(num_now < 8){
                    num.text(++num_now);
                }
            });
            $('.minus').on('click',function(){
                var num = $(this).parent().children('.num');
                var num_now = parseInt(num.text());
                if(num_now > 0){
                    num.text(--num_now);
                }
            });
            $('#teacher-view-submit').on('click',function(){
                var score = <?php echo $res['score']; ?>,id = <?php echo $_GET['exam_id']; ?>;
                $.each($('.num'),function(i,v){
                    score += parseInt($(this).text());
                });
                console.log(id);
                $.post('./enter.php',{url_action:'teacher_view_submit',id:id,score:score},function(data){
                    if(data.code){
                        layer.msg(data.msg);
                        setTimeout(function(){
                            location.href = './teacher-exam.php';
                        },2000);
                    }else{
                        layer.msg(data.msg);
                    }
                },'JSON');
            });
        </script>
    </body>
</html>